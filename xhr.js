function login() {
    let xhr = new XMLHttpRequest();
    xhr.open('POST', 'https://reqres.in/api/login');

    let json = JSON.stringify({
        "username": "george.bluth@reqres.in",
        "email": "george.bluth@reqres.in",
        "password": "george"
    });
    xhr.setRequestHeader('Content-Type', 'application/json;charset=utf-8');

    xhr.send(json);

    xhr.onloadend = () => getUsers(xhr.responseText);
}

login();

function getUsers(response) {
    let xhr = new XMLHttpRequest();
    xhr.open('GET','https://reqres.in/api/users');

    xhr.setRequestHeader('Cache-Control', 'no-cache');
    xhr.setRequestHeader('Accept-Language', 'ru-RU');
    xhr.setRequestHeader('Content-Type', 'application/json;charset=utf-8');
    xhr.setRequestHeader('Authorization', response);
    xhr.responseType = 'json';

    xhr.send();

    xhr.onloadend = () => {
        let result = xhr.response;
        result.data.forEach((user) => console.log(user.email));
    };

    xhr.onreadystatechange = () => {
      if (xhr.readyState == 1) console.log('Request called');
      if (xhr.readyState == 2) console.log('Headers received');
      if (xhr.readyState == 3) console.log('Loading body');
      if (xhr.readyState == 4) console.log('Request done');
    };
}